# A + B program
## Usage
### C++
Compile program using g++.
```bash
g++ main.cpp -O main.out 
```

Run program.
```bash
./main.out
```
### Python
Run program.
```bash
python main.py  # You can use python3
```
